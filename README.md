# Most Popular News

A simple news application using MVP architecture that retrieves most popular news from NY times.
Version 1.0

## Getting Started

Clone the project from Git repo and apply the two steps below:
- Sync Gradle
- Rebuild Project

### Prerequisites

[Android Studio](https://developer.android.com/studio/)

### How to run/install the app

Click on Run button, you should be able to choose between a connected android device or a configured emulator.

## How to run the instrumented test

Right click the RecyclerViewInstrumentedTest.java and click Run 'RecyclerViewInstrumentedTest'

### Instrumented test description

The instrumented test consists of the below 
- Click on each item in the list (up to 3 items) and go back once details screen is shown
- Swipe to refresh and fetch news

## How to run the unit test

Right click the FetchNewsUnitTest.java and click Run 'FetchNewsUnitTest'

### Unit test description

The unit test consists of the below 
- Call API to fetch news from and check response

### How to generate a coverage report

To get a coverage report, execute the below command in Terminal window of Android Studio:

```
./gradlew createDebugCoverageReport
```

The report will be generated at the following path:
app/build/outputs/reports/coverage/debug/

## Author

Jose Daccache
