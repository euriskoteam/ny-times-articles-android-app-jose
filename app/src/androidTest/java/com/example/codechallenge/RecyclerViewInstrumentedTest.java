package com.example.codechallenge;

import android.view.View;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.codechallenge.views.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class RecyclerViewInstrumentedTest {

    private static final int SLEEP_TIME_BEFORE_VARIABLE = 3000;
    private static final int SLEEP_TIME_AFTER_VARIABLE = 5000;
    private static final int ITEMS_TO_TRY_VARIABLE = 3;
    private static final int SWIPE_PERCENTAGE_REFRESH = 90;

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    public static ViewAction withCustomConstraints(final ViewAction action, final Matcher<View> constraints) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return constraints;
            }

            @Override
            public String getDescription() {
                return action.getDescription();
            }

            @Override
            public void perform(UiController uiController, View view) {
                action.perform(uiController, view);
            }
        };
    }


    /**
     * @param RecyclerViewId recylerview resource id
     * @return recyclerview items count
     */
    public static int getRecyclerViewItemsCount(@IdRes int RecyclerViewId) {
        final int[] itemsCount = {0};
        Matcher matcher = new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                itemsCount[0] = ((RecyclerView) item).getAdapter().getItemCount();
                return true;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
        onView(allOf(withId(RecyclerViewId), isDisplayed())).check(matches(matcher));
        return itemsCount[0];
    }

    /**
     * run application process test
     */
    @Test
    public void runTest() {
        sleep(SLEEP_TIME_BEFORE_VARIABLE);
        int itemsCount = getRecyclerViewItemsCount(R.id.rvNews);
        ViewInteraction newsRecyclerView = onView(withId(R.id.rvNews));

        for (int i = 0; i < itemsCount; i++) {
            if (i + 1 > ITEMS_TO_TRY_VARIABLE)
                break;
            newsRecyclerView.perform(RecyclerViewActions.actionOnItemAtPosition(i, click()));
            sleep(SLEEP_TIME_AFTER_VARIABLE);
            pressBack();
        }
        onView(withId(R.id.swipeContainer))
                .perform(withCustomConstraints(swipeDown(), isDisplayingAtLeast(SWIPE_PERCENTAGE_REFRESH)));
        sleep(SLEEP_TIME_AFTER_VARIABLE);
    }

    public void sleep(long timeInMillis) {
        try {
            Thread.sleep(timeInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
