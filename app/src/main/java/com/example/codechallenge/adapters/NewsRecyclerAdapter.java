package com.example.codechallenge.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.codechallenge.R;
import com.example.codechallenge.interfaces.NewsRowView;
import com.example.codechallenge.presenters.NewsListPresenter;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.NewsViewHolder> {


    private NewsListPresenter presenter;
    private RecyclerViewClickListener itemClickListener;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public NewsRecyclerAdapter(RecyclerViewClickListener itemClickListener, NewsListPresenter repositoriesPresenter, ImageLoader imageLoader, DisplayImageOptions options) {
        this.presenter = repositoriesPresenter;
        this.itemClickListener = itemClickListener;
        this.imageLoader = imageLoader;
        this.options = options;

    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View view, int position);
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_row_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        presenter.onBindNewsRowViewAtPosition(position, holder);

    }

    @Override
    public int getItemCount() {
        return presenter.getNewsRowsCount();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder implements NewsRowView, View.OnClickListener {

        TextView titleTextView, publishDateTextView, authorTextView;
        CircularImageView newsImageIcon;

        private NewsViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.newsTitle);
            publishDateTextView = itemView.findViewById(R.id.newsDate);
            authorTextView = itemView.findViewById(R.id.newsAuthor);
            newsImageIcon = itemView.findViewById(R.id.newsImage);
            itemView.setOnClickListener(this);

        }

        @Override
        public void setTitle(String title) {
            if (title.isEmpty())
                titleTextView.setVisibility(View.GONE);
            titleTextView.setText(title);
        }

        @Override
        public void setPublishDate(String publishDate) {
            if (publishDate.isEmpty())
                publishDateTextView.setVisibility(View.GONE);
            publishDateTextView.setText(publishDate);
        }

        @Override
        public void setAuthor(String author) {
            if (author.isEmpty())
                authorTextView.setVisibility(View.GONE);
            authorTextView.setText(author);
        }

        @Override
        public void setIcon(String url) {
            imageLoader.displayImage(url, newsImageIcon, options);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                itemClickListener.recyclerViewListClicked(view, getAdapterPosition());
            }
        }
    }
}


