package com.example.codechallenge.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.codechallenge.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class LocalUtils {

    public final static String BASE_URL = "https://api.nytimes.com";
    public final static String GET_NEWS_URL = "svc/mostpopular/v2/mostviewed/all-sections/7.json";
    public final static String API_KEY = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5";

    public static DisplayImageOptions getImageLoaderOptions() {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .showImageOnFail(R.drawable.ic_error_black)
                .showImageForEmptyUri(R.drawable.ic_error_black)
                .cacheOnDisk(true).build();
    }

    public static ImageLoader getImageLoader(Context context) {
        ImageLoader imgLoader = ImageLoader.getInstance();
        imgLoader.init(ImageLoaderConfiguration.createDefault(context));

        return ImageLoader.getInstance();
    }

}
