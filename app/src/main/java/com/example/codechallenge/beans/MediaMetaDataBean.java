package com.example.codechallenge.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MediaMetaDataBean implements Serializable {
    @SerializedName("url")
    private String url = "";
    @SerializedName("format")
    private String format = "";
    @SerializedName("height")
    private int height = -1;
    @SerializedName("width")
    private int width = -1;

    public String getUrl() {
        return url;
    }

    public String getFormat() {
        return format;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
