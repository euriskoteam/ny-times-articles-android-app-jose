package com.example.codechallenge.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MediaBean implements Serializable {
    @SerializedName("type")
    private String type = "";
    @SerializedName("subtype")
    private String subtype = "";
    @SerializedName("caption")
    private String caption = "";
    @SerializedName("copyright")
    private String copyright = "";
    @SerializedName("approved_for_syndication")
    int approved_for_syndication = -1;
    @SerializedName("url")
    private String url = "";
    @SerializedName("media-metadata")
    private ArrayList<MediaMetaDataBean> mediaMetaData = new ArrayList<>();

    public String getType() {
        return type;
    }

    public String getSubtype() {
        return subtype;
    }

    public String getCaption() {
        return caption;
    }

    public String getCopyright() {
        return copyright;
    }

    public int getApproved_for_syndication() {
        return approved_for_syndication;
    }

    public String getUrl() {
        return url;
    }

    public ArrayList<MediaMetaDataBean> getMediaMetaData() {
        return mediaMetaData;
    }
}
