package com.example.codechallenge.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ResponseBeans implements Serializable {

    @SerializedName("status")
    private String status = "";
    @SerializedName("copyright")
    private String copyright = "";
    @SerializedName("num_results")
    private int numResults = -1;
    @SerializedName("results")
    private ArrayList<ResultsBean> results = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public String getCopyright() {
        return copyright;
    }

    public int getNumResults() {
        return numResults;
    }

    public ArrayList<ResultsBean> getResults() {
        return results;
    }
}
