package com.example.codechallenge.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ResultsBean implements Serializable {

    @SerializedName("url")
    private String url = "";
    @SerializedName("adx_keywords")
    private String adxKeywords = "";
    @SerializedName("column")
    private String column = "";
    @SerializedName("section")
    private String section = "";
    @SerializedName("byline")
    private String byline = "";
    @SerializedName("type")
    private String type = "";
    @SerializedName("title")
    private String title = "";
    @SerializedName("abstract")
    private String abstractDescription = "";
    @SerializedName("published_date")
    private String publishedDate = "";
    @SerializedName("source")
    private String source = "";
    @SerializedName("id")
    private Long id = 0L;
    @SerializedName("asset_id")
    private Long assetId = 0L;
    @SerializedName("views")
    private int views = -1;
    @SerializedName("media")
    private ArrayList<MediaBean> media = new ArrayList<>();

    public String getUrl() {
        return url;
    }

    public String getAdxKeywords() {
        return adxKeywords;
    }

    public String getColumn() {
        return column;
    }

    public String getSection() {
        return section;
    }

    public String getByline() {
        return byline;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getAbstractDescription() {
        return abstractDescription;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public String getSource() {
        return source;
    }

    public Long getId() {
        return id;
    }

    public Long getAssetId() {
        return assetId;
    }

    public int getViews() {
        return views;
    }

    public ArrayList<MediaBean> getMedia() {
        return media;
    }
}
