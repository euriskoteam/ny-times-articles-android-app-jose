package com.example.codechallenge.presenters;

import com.example.codechallenge.beans.ResultsBean;
import com.example.codechallenge.interfaces.NewsRowView;

import java.util.ArrayList;
import java.util.List;

public class NewsListPresenter {

    public void setNews(List<ResultsBean> news) {
        this.news = news;
    }

    private List<ResultsBean> news = new ArrayList<>();

    public void onBindNewsRowViewAtPosition(int position, NewsRowView rowView) {
        ResultsBean resultNews = news.get(position);
        rowView.setAuthor(resultNews.getByline());
        rowView.setTitle(resultNews.getTitle());
        rowView.setPublishDate(resultNews.getPublishedDate());
        rowView.setIcon(resultNews.getMedia().get(0).getMediaMetaData().get(0).getUrl());
    }

    public int getNewsRowsCount() {
        return news.size();
    }

    public ResultsBean getNewsAtPosition(int position) {
        return news.get(position);
    }

}
