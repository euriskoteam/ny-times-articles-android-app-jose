package com.example.codechallenge.presenters;

import com.example.codechallenge.beans.ResultsBean;

public class NewsDetailsPresenter {

    private ResultsBean resultBean;
    private View view;


    public NewsDetailsPresenter(View view, ResultsBean resultBean) {
        this.resultBean = resultBean;
        this.view = view;
    }

    public void updateTitle() {
        view.updateResultTitle(resultBean.getTitle());
    }

    public void updateDescription() {
        view.updateResultDescription(resultBean.getAbstractDescription());
    }

    public void updateImage() {
        view.updateImage(resultBean.getMedia().get(0).getMediaMetaData().get(0).getUrl());
    }

    public void updateCaption() {
        view.updateCaption(resultBean.getMedia().get(0).getCaption() + "\n" + resultBean.getMedia().get(0).getCopyright());
    }

    public void updatePublishDate() {
        view.updatePublishDate(resultBean.getPublishedDate());
    }

    public interface View {
        void updateResultTitle(String title);
        void updateResultDescription(String description);
        void updateImage(String url);
        void updateCaption(String caption);
        void updatePublishDate(String publishDate);
    }

}
