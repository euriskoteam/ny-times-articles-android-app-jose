package com.example.codechallenge.interfaces;

import com.example.codechallenge.beans.ResponseBeans;
import com.example.codechallenge.utils.LocalUtils;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET(LocalUtils.GET_NEWS_URL)
    Call<ResponseBeans> getNews(@Query("api-key") String apiKey);
}
