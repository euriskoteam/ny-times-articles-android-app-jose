package com.example.codechallenge.interfaces;

public interface NewsRowView {
    void setTitle(String title);
    void setAuthor(String author);
    void setPublishDate(String publishDate);
    void setIcon(String url);
}
