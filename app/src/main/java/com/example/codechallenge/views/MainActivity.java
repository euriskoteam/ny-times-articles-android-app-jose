package com.example.codechallenge.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.codechallenge.R;
import com.example.codechallenge.adapters.NewsRecyclerAdapter;
import com.example.codechallenge.beans.ResponseBeans;
import com.example.codechallenge.beans.ResultsBean;
import com.example.codechallenge.interfaces.APIInterface;
import com.example.codechallenge.networking.RetrofitInstance;
import com.example.codechallenge.presenters.NewsListPresenter;
import com.example.codechallenge.utils.LocalUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NewsRecyclerAdapter.RecyclerViewClickListener {

    private RecyclerView rvNews;
    private NewsListPresenter listPresenter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Call<ResponseBeans> call;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listPresenter = new NewsListPresenter();
        imageLoader = LocalUtils.getImageLoader(this);
        options = LocalUtils.getImageLoaderOptions();

        rvNews = findViewById(R.id.rvNews);
        NewsRecyclerAdapter adapter = new NewsRecyclerAdapter(this, listPresenter, imageLoader, options);
        LinearLayoutManager llManager = new LinearLayoutManager(this);
        rvNews.setLayoutManager(llManager);
        rvNews.setAdapter(adapter);

        mSwipeRefreshLayout = findViewById(R.id.swipeContainer);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewsAPICall();
            }
        });
        getNewsAPICall();
    }

    private void getNewsAPICall() {
        mSwipeRefreshLayout.setRefreshing(true);
        APIInterface service = RetrofitInstance.getRetrofitInstance().create(APIInterface.class);
        call = service.getNews(LocalUtils.API_KEY);
        call.enqueue(new Callback<ResponseBeans>() {
            @Override
            public void onResponse(Call<ResponseBeans> call, Response<ResponseBeans> response) {
                mSwipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setupView(response.body().getResults());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBeans> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                call.cancel();
                Toast.makeText(MainActivity.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupView(ArrayList<ResultsBean> result) {
        listPresenter.setNews(result);
        rvNews.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void recyclerViewListClicked(View view, int position) {
        ResultsBean rb = listPresenter.getNewsAtPosition(position);
        gotToDetailsActivity(rb);
    }

    private void gotToDetailsActivity(ResultsBean rowBean) {
        Intent intent = new Intent(this, NewsDetailsActivity.class);
        intent.putExtra("result", rowBean);
        startActivity(intent);
    }

    @Override
    protected void onStop() {
        if (call != null)
            call.cancel();
        super.onStop();
    }
}
