package com.example.codechallenge.views;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.codechallenge.R;
import com.example.codechallenge.beans.ResultsBean;
import com.example.codechallenge.presenters.NewsDetailsPresenter;
import com.example.codechallenge.utils.LocalUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class NewsDetailsActivity extends AppCompatActivity implements NewsDetailsPresenter.View {

    private TextView titleView, descriptionView, captionView, publishDateView;
    private ImageView imageDetailsView;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        imageDetailsView = findViewById(R.id.ivNewsDetails);
        titleView = findViewById(R.id.newsDetailsTitle);
        descriptionView = findViewById(R.id.newsDetailsDescription);
        captionView = findViewById(R.id.newsDetailsCaption);
        publishDateView = findViewById(R.id.newsDetailsPublishDate);
        imageLoader = LocalUtils.getImageLoader(this);
        options = LocalUtils.getImageLoaderOptions();

        ResultsBean result = (ResultsBean) getIntent().getExtras().getSerializable("result");
        if (result != null) {
            NewsDetailsPresenter presenter = new NewsDetailsPresenter(this, result);
            presenter.updateTitle();
            presenter.updateDescription();
            presenter.updateImage();
            presenter.updateCaption();
            presenter.updatePublishDate();
        }
    }

    @Override
    public void updateResultTitle(String title) {
        titleView.setText(title);
    }

    @Override
    public void updateResultDescription(String description) {
        descriptionView.setText(description);
    }

    @Override
    public void updateImage(String url) {
        imageLoader.displayImage(url, imageDetailsView, options);
    }

    @Override
    public void updateCaption(String caption) {
        captionView.setText(caption);
    }


    @Override
    public void updatePublishDate(String publishDate) {
        publishDateView.setText(publishDate);
    }
}
