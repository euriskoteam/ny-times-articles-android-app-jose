package com.example.codechallenge;

import com.example.codechallenge.beans.ResponseBeans;
import com.example.codechallenge.interfaces.APIInterface;
import com.example.codechallenge.networking.RetrofitInstance;
import com.example.codechallenge.utils.LocalUtils;

import org.junit.Test;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class FetchNewsUnitTest {

    @Test
    public void getNews() {

        APIInterface apiEndpoints = RetrofitInstance.getRetrofitInstance().create(APIInterface.class);
        Call<ResponseBeans> call = apiEndpoints.getNews(LocalUtils.API_KEY);

        try {
            //Magic is here at .execute() instead of .enqueue()
            Response<ResponseBeans> response = call.execute();

            assertTrue(response.isSuccessful());

        } catch (IOException e) {
            fail();
            e.printStackTrace();
        }

    }
}